FROM openjdk:8
ADD target/springbootdockerhub.jar springbootdockerhub.jar
EXPOSE 8081
ENTRYPOINT ["java","-jar","springbootdockerhub.jar"]